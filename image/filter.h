#ifndef FILTER_H
#define FILTER_H

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <vector>
#include "Header.h"
#include "Pixel.h"
#include "Image.h"

using namespace std;
using Matrix = vector<vector<int>>;

namespace Filter {
  extern Matrix K3, K5;
  Image& sharpen(Image&, Matrix&);
  Pixel apply_kernel(Image&, int x, int y, Matrix&);
  int clamp(int, int, int);
}
#endif
