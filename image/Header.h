/****************************************
 * Matthew Balcewicz, Taylor Horst
 * CPSC 1020 001, Sp18
 * Mbalcew@g.clemson.edu, thorst@g.clemson.edu
 ****************************************/

#ifndef HEADER_T
#define HEADER_T

#include <string>
#include <iostream>

using namespace std;

class Header {
private:
    std::string MAGIC;
    int W, H, MC;

public:
    //default constructor
    Header() : MAGIC(""), W(0), H(0), MC(0) {}

    //copy constructor
    Header(const Header &HDR) : MAGIC(HDR.MAGIC), W(HDR.W), H(HDR.H), MC(HDR.MC) {}

    //param constructor
    Header(std::string m, int w, int h, int mc) : MAGIC(m), W(w), H(h), MC(mc) {}

    ~Header() {};

    // Value getter, use only these for immutable headers
    std::string magic() const;

    int width() const;

    int height() const;

    int max_color() const;

    // Setter, use for mutable headers - or use references!
    void set_magic(string);

    void set_width(int);

    void set_height(int);

    void set_max_color(int);

    // Assignment
    Header &operator=(const Header &);

    // Output
    friend std::ostream &operator<<(std::ostream &, const Header &);
};

#endif
