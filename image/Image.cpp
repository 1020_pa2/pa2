/****************************************
 * Matthew Balcewicz, Taylor Horst
 * CPSC 1020 001, Sp18
 * Mbalcew@g.clemson.edu, thorst@g.clemson.edu
 ****************************************/

#include "Image.h"

Header Image::read_header(ifstream &in) {
    string magic;
    int w, h, mc;

    in >> magic;
    Image::ignore_comments(in);
    in >> w;
    Image::ignore_comments(in);
    in >> h;
    Image::ignore_comments(in);
    in >> mc;
    in.ignore(256, '\n');
    return Header(magic, w, h, mc);
}

void Image::ignore_comments(ifstream &in) {
    char c;
    in.get(c);

    while (isspace(c) || c == '#') {
        if (c == '#')
            in.ignore(256, '\n');
        in.get(c);
    }

    in.unget();
}

// This function allocates memory!
std::vector<Pixel> Image::read_pixels(const Header &hdr, ifstream &in) {
    int num_pixels = hdr.width() * hdr.height();
    std::vector<Pixel> pixels;

    if (hdr.magic() == "P3") {
        uint r, g, b;
        for (int i = 0; i < num_pixels; i++) {
            in >> r >> g >> b;
            pixels.push_back(Pixel(r, g, b)); // Assignment operator to the rescue again!
        }
    } else {
        uint8_t r, g, b;
        for (int i = 0; i < num_pixels; i++) {
            r = in.get();
            g = in.get();
            b = in.get();
            pixels.push_back(Pixel(r, g, b));
        }
    }

    return pixels;
}

// accessors
const Header &Image::header() const { return this->HDR; }

const std::vector<Pixel>& Image::pixels() const { return this->PIX; }

// If someone wants to change the header, the Image controls
// which fields it will to expose
void Image::make_p3() { this->HDR.magic() = "P3"; }

void Image::make_p6() { this->HDR.magic() = "P6"; }

void Image::write_header(ofstream &out) const {
    out << this->HDR.magic() << " "
        << this->HDR.width() << " "
        << this->HDR.height() << " "
        << this->HDR.max_color() << "\n";
}

void Image::write_to(ofstream &out) const {
    write_header(out);

    if (this->HDR.magic() == "P3") {
        for (auto pix : this->PIX) {
            out << (int) pix.r() << ' '
                << (int) pix.g() << ' '
                << (int) pix.b() << ' ';
        }
    } else {
        for (auto pix : this->PIX) {
            out << pix.r() << pix.g() << pix.b();
        }
    }
}

// This function is important!
Image &Image::operator=(const Image &rhs) {
    if (this == &rhs) return *this; // Why do we need this? Hint: delete[]
    // Header is simple
    this->HDR = rhs.HDR;  // Assignment operator

    this->PIX.erase(this->PIX.cbegin(), this->PIX.cend());

    for(auto pix : rhs.PIX){
      this->PIX.push_back(pix);
    }

    return *this;
}

// Get one pixel
Pixel &Image::operator()(int x, int y) {
    int ndx = (this->HDR.width() * y) + x;
    return this->PIX[ndx];
}
