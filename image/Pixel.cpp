/****************************************
 * Matthew Balcewicz, Taylor Horst
 * CPSC 1020 001, Sp18
 * Mbalcew@g.clemson.edu, thorst@g.clemson.edu
 ****************************************/

#include "Pixel.h"

// Getters
uint8_t Pixel::r() const { return this->R; }

uint8_t Pixel::g() const { return this->G; }

uint8_t Pixel::b() const { return this->B; }

// Setters
void Pixel::set_r(uint8_t r) { this->R = r; }

void Pixel::set_g(uint8_t g) { this->G = g; }

void Pixel::set_b(uint8_t b) { this->B = b; }

// Assignment
Pixel &Pixel::operator=(const Pixel &rhs) {
    if (this == &rhs) return *this; // What does this line do?
    this->R = rhs.R;
    this->G = rhs.G;
    this->B = rhs.B;
    return *this;
}

// Output
std::ostream &operator<<(std::ostream &out, const Pixel &P) {
    return out << "("
               << P.R << " "
               << P.G << " "
               << P.B << ")";
}
