#include "filter.h"

Matrix Filter::K3 =
        {{0, -1, 0},
         {-1, 5, -1},
         { 0, -1, 0}};

Matrix Filter::K5 =
        {{0,  0,  -1, 0,  0},
         {0,  0,  -1, 0,  0},
         {-1, -1, 9,  -1, -1},
         {0,  0,  -1, 0,  0},
         {0,  0,  -1, 0,  0}};
/*
Matrix Filter::I =
        {{0, 0,  0},
         {0, -1, 0},
         {0, 0,  0}};
*/

//Applies a sharpen effect to an image taking in the image and a Filter
//for which the filter will be used to calculate the rgb values of
//the output image
Image & Filter::sharpen(Image &img, Matrix &k) {
    if (!(&k == &Filter::K3 || &k == &Filter::K5)) {
        return img;
    }

    //will either be 1 or 2 depending on filter used
    //this is for ignoring the borders of the image because
    //the filter cannot go out of the bounds of the image
    int k_center = k.size() / 2;

    //Copy reference of the base image
    Image& copy(img);

    //loop for calculating the rgb values in each pixel by going through each
    //row
    for (int i = k_center; i < img.header().height() - k_center; i++) {
        for (int j = k_center; j < img.header().width() - k_center; j++) {
          //sets the pixel at the pixel location for the copy image to
          //values set by the apply kernel function
            copy(j, i) = Filter::apply_kernel(img, j, i, k);
        }
    }

    return copy;
}

Pixel Filter::apply_kernel(Image &img, int x, int y, Matrix & k) {
    int r = 0, g = 0, b = 0;
    //determine if k3 or k5
    if(&k == &Filter::K3) {
        //calculate new pixel values and call clamp on each value
    //r
        for(int i = y - 1; i < y+1; i++){
          for(int j = x - 1; j < x+1; j++){
            if(i == y && j == x){
              r += img(j,i).r()*5;
              g += img(j,i).g()*5;
              b += img(j,i).b()*5;
            }
            else if(i == y || j == x){
              r += img(j,i).r()*-1;
              g += img(j,i).g()*-1;
              b += img(j,i).b()*-1;
            }
          }
        }

        r = Filter::clamp(0, 255, r);
        g = Filter::clamp(0, 255, g);
        b = Filter::clamp(0, 255, b);
    }
    else if(&k == &Filter::K5) {
        //calculate new pixel values and call clamp on each value
    //r
    for(int i = y - 1; i < y+1; i++){
      for(int j = x - 1; j < x+1; j++){
        if(i == y && j == x){
          r += img(j,i).r()*9;
          g += img(j,i).g()*9;
          b += img(j,i).b()*9;
        }
        else if(i == y || j == x){
          r += img(j,i).r()*-1;
          g += img(j,i).g()*-1;
          b += img(j,i).b()*-1;
        }
      }
    }

    r = Filter::clamp(0, 255, r);
    g = Filter::clamp(0, 255, g);
    b = Filter::clamp(0, 255, b);
    }
    //return
    return Pixel(r, g, b);
}

//makes sure that the rgb values stay within 0-255
int Filter::clamp(int lo, int hi, int x) {
    return std::max(lo, std::min(x, hi));
}
