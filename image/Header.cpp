/****************************************
 * Matthew Balcewicz, Taylor Horst
 * CPSC 1020 001, Sp18
 * Mbalcew@g.clemson.edu, thorst@g.clemson.edu
 ****************************************/

#include "Header.h"

// Simple getter
std::string Header::magic() const { return this->MAGIC; }

int Header::width() const { return this->W; }

int Header::height() const { return this->H; }

int Header::max_color() const { return this->MC; }

// Simple setter
void Header::set_magic(string magic) { this->MAGIC = magic; }

void Header::set_width(int w) { this->W = w; }

void Header::set_height(int h) { this->H = h; }

void Header::set_max_color(int mc) { this->MC = mc; }

// Assignment
Header &Header::operator=(const Header &hdr) {
    if (this == &hdr) return *this;  // What does this line do?
    this->MAGIC = hdr.MAGIC;
    this->W = hdr.W;
    this->H = hdr.H;
    this->MC = hdr.MC;
    return *this;
}

// Output
std::ostream &operator<<(std::ostream &out, const Header &hdr) {
    return out << hdr.magic() << std::endl
               << hdr.width() << std::endl
               << hdr.height() << std::endl
               << hdr.max_color() << std::endl;
}
