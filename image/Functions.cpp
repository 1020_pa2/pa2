#include "Functions.h"


//Function to remove noise from an image by taking the average values of
//the pixels of each image
Image Functions::removeNoiseAverage(vector<Image> &img){
	//variable declaration
    vector<Pixel> out_pixels;
    int temp_r, temp_g, temp_b;

    //loops through the pixels first
    for(int pix_count = 0; (unsigned int)pix_count < img.at(0).pixels().size(); pix_count++){
        temp_r = 0;
        temp_g = 0;
        temp_b = 0;
        //sums up the pixels from each image
        for(int img_count = 0; (unsigned int)img_count < img.size(); img_count++){
            temp_r += img.at(img_count).pixels().at(pix_count).r();
            temp_g += img.at(img_count).pixels().at(pix_count).g();
            temp_b += img.at(img_count).pixels().at(pix_count).b();
        }
        //takes the average for each value of r, g, and b
        temp_r /= img.size();
        temp_g /= img.size();
        temp_b /= img.size();
        out_pixels.push_back(Pixel(temp_r, temp_g, temp_b));
    }

	//create the output img
	Image output(img.at(0).header(), out_pixels);

	return output;
}

//Function to remove an object from an image by median pixel calculation
//across each image
Image Functions::removeNoiseMedian(const vector<Image> &img){
	//variable declaration
	int median = img.size() / 2;
	vector<uint8_t> red(img.size(), 0);
	vector<uint8_t> green(img.size(), 0);
	vector<uint8_t> blue(img.size(), 0);
	vector<Pixel> output_pix;

	//loops through the pixels intially
	for(int pix_count = 0; pix_count < (int)img.at(0).pixels().size(); pix_count++) {
		//loops through each image to add each value to a vector
		for(int img_count = 0; img_count < (int)img.size(); img_count++) {
			red.at(img_count) =
				img.at(img_count).pixels().at(pix_count).r();
			green.at(img_count) =
				img.at(img_count).pixels().at(pix_count).g();
			blue.at(img_count) =
				img.at(img_count).pixels().at(pix_count).b();
		}
		//find the mean and move the rgb values to output by sorting the vector,
    //then taking the median of the vector
		std::sort(red.begin(), red.end());
		std::sort(green.begin(), green.end());
		std::sort(blue.begin(), blue.end());

		output_pix.push_back(Pixel(red.at(median), green.at(median), blue.at(median)));
	}

	Image output(img.at(0).header(), output_pix);

	return output;
}

//Function for opening input files
vector<ifstream*> Functions::openInputFiles(std::string name) {
	//variable declaration
	int file_num = 0;
	stringstream file_name;
	vector<ifstream*> files;

	//change the file_num based on the name variable
	if(name.compare("median") == 0)
		file_num = 9;
	else if(name.compare("average") == 0)
		file_num = 10;

	//create the files
	for(int count = 1; count <= file_num; count++) {
        file_name.str("");
        file_name << name << "_" << setw(3) << setfill('0') << count << ".ppm";
        files.push_back(new ifstream(file_name.str()));
    }

	return files;
}
