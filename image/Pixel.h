/****************************************
 * Matthew Balcewicz, Taylor Horst
 * CPSC 1020 001, Sp18
 * Mbalcew@g.clemson.edu, thorst@g.clemson.edu
 ****************************************/

#ifndef PIXEL_H
#define PIXEL_H

#include <ostream>

using namespace std;

class Pixel {
private:
    uint8_t R, G, B;

public:
    //default constructor
    Pixel() : R(0), G(0), B(0) {}

    //copy constructor
    Pixel(const Pixel &P) : R(P.R), G(P.G), B(P.B) {}

    //param constructor
    Pixel(uint8_t r, uint8_t g, uint8_t b) : R(r), G(g), B(b) {}

    ~Pixel() {};

    // Plain getters, use only these for immutable pixels
    uint8_t r() const;

    uint8_t g() const;

    uint8_t b() const;

    // Plain setters, use these for mutable pixels
    void set_r(uint8_t);

    void set_g(uint8_t);

    void set_b(uint8_t);

    // Assignment operator
    Pixel &operator=(const Pixel &);

    // Output operator
    friend std::ostream &operator<<(std::ostream &, const Pixel &);
};

#endif
