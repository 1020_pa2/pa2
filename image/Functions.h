/****************************************
 * Matthew Balcewicz, Taylor Horst
 * CPSC 1020 001, Sp18
 * Mbalcew@g.clemson.edu, thorst@g.clemson.edu
 ****************************************/
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include "Image.h"

namespace Functions {
	//removeNoiseAverage can be used with a vector of any size and 
	//assumes that all images in the vector have the same number of pixels 
	Image removeNoiseAverage(vector<Image> &img);
	
	//removeNoiseMedian can be used with a vector of any size and
	//assumes that all images in the vector have the same number of pixels
	Image removeNoiseMedian(vector<Image> const &img);

	//opens files	
	vector<ifstream*> openInputFiles(std::string name);
}

#endif
