/****************************************
 * Matthew Balcewicz, Taylor Horst
 * CPSC 1020 001, Sp18
 * Mbalcew@g.clemson.edu, thorst@g.clemson.edu
 ****************************************/

#include <iostream>
#include <string>
#include "image/Pixel.h"
#include "image/Header.h"
#include "image/Image.h"
#include "image/filter.h"
#include "image/Functions.h"

int main(int argc, char const *argv[]) {
    string arg1 = argv[1];

    //Checks to see how many command line arguements are made and exits
    //if not correct
    if (argc != 3) {
        std::cerr << "USAGE: ./out <input.ppm> <output.ppm>";
        return 1;
    }

    //Checks to remove object from an image
    if (arg1.compare("median") == 0) {
        //vars
        vector<Image> input_imgs;

        //files
        vector<std::ifstream*> files = Functions::openInputFiles("median");
        std::ofstream f_output;
        f_output.open(argv[2]);

        //read in the images
        for(int count = 0; (unsigned int)count < files.size(); count++) {
            input_imgs.push_back(Image(*files.at(count)));
        }

        //remove noise
        Image output = Functions::removeNoiseMedian(input_imgs);

        //write to output file
        output.write_to(f_output);

        //close files
        for(std::ifstream * in : files) {
            in->close();
        }
        f_output.close();

    }

    //Checks to remove noise from an image
    else if (arg1.compare("average") == 0) {
        //vars
        vector<Image> input_imgs;

        //files
        vector<std::ifstream*> files = Functions::openInputFiles("average");
        std:: ofstream f_output;
        f_output.open(argv[2]);

        //read in images
        for(int count = 0; (unsigned int)count < files.size(); count++) {
            input_imgs.push_back(Image(*files.at(count)));
        }

        //remove noise
        Image output = Functions::removeNoiseAverage(input_imgs);

        //write to output file
        output.write_to(f_output);

        //close files
        for(std::ifstream* in : files) {
            in->close();
        }
        f_output.close();
    }

    //Otherwise, the sharpen filters will be applied here
    else {
      //sets up a naming scheme for the output files
        std::string k3s = "k3_";
        std::string k5s = "k5_";
        std::string outname = argv[2];
        k3s.append(outname);
        k5s.append(outname);

        // Open files
        std::ifstream in(argv[1]);
        std::ofstream out1(k3s);
        std::ofstream out2(k5s);

        if (!(in && out1 && out2)) {
            std::cerr << "Could not open input/output\n";
            return 1;
        }

        //Brings in the input image into image object
        Image puppy(in);

        //Applies the sharpen effect to the images
        Image k3 = puppy;
        Image k5 = puppy;

        k3 = Filter::sharpen(k3, Filter::K3);
        k5 = Filter::sharpen(k5, Filter::K5);

        //Writes out the images so that they will produce a sharpened image
        k3.write_to(out1);
        k5.write_to(out2);

        //Closes all files
        in.close();
        out1.close();
        out2.close();

    }

    return 0;
}
